package core;


import java.io.IOException;

import org.junit.After;
import org.junit.Before;

import pages.LoginPage;

/**
 * Class base contendo os m�todos para inicializa��o e finaliza��o de cada teste
 * 
 * */
public class BaseTest {
	private LoginPage page = new LoginPage();
	
	@Before
	public void inicializa(){
		//Preferi setar aqui driver ao inv�s de config em vari�veis de ambiente
		System.setProperty("webdriver.gecko.driver", "C://geckodriver/geckodriver.exe");
		
		page.acessarTelaInicial();
		page.setEmail("admin");
		page.setSenha("admin");
		page.entrar();
	}
	
	@After
	public void finaliza() throws IOException{	
		if(Propriedades.FECHAR_BROWSER) {
			DriverFactory.killDriver();
		}
	}

}
