package core;

/**
 * Class que configura qual navegador deseja usar
 * */
public class Propriedades {
	
	public static boolean FECHAR_BROWSER = true;
	
	public static Browsers browser = Browsers.FIREFOX;
	
	public enum Browsers {
		CHROME,
		FIREFOX
	}

}
