package core;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

/**
 * Class base contendo os principais m�todos para execu��o dos testes
 * 
 * */
public class BasePage {

	/* ******** TextField e TextArea ************/
	
	public void escrever(By by, String texto){
		DriverFactory.getDriver().findElement(by).click();
		DriverFactory.getDriver().findElement(by).clear();
		DriverFactory.getDriver().findElement(by).sendKeys(texto);
	}

	public void escreverID(String idCampo, String texto){
		escrever(By.id(idCampo), texto);
	}
	public void escreverPorName(String nameCampo, String texto){
		escrever(By.name(nameCampo), texto);
	}
	public void escreverPorXpath(String xpathCampo, String texto){
		escrever(By.xpath(xpathCampo), texto);
	}
	
	public String obterValorCampo(String id_campo) {
		return DriverFactory.getDriver().findElement(By.id(id_campo)).getAttribute("value");
	}
	 
	/* ********* Clicar ***********/
	public void clicar(String xpath) {
		DriverFactory.getDriver().findElement(By.xpath(xpath)).click();
	}
	
	public void setData(String data) {
		DriverFactory.getDriver().findElement(By.xpath("//a[.="+data+"]")).click();
	}
	
	/* ******** Combo ************/
	
	public void selecionarCombo(String id, String valor) {
		WebElement element = DriverFactory.getDriver().findElement(By.id(id));
		Select combo = new Select(element);
		combo.selectByVisibleText(valor);
	}
	
	/* ******** Botao ************/
	
	public void clicarBotao(By by) {
		DriverFactory.getDriver().findElement(by).click();
	}
	public void clicarBotao(String id) {
		clicarBotao(By.id(id));
	}
	
	public String obterValueElemento(String id) {
		return DriverFactory.getDriver().findElement(By.id(id)).getAttribute("value");
	}
	
	/* ******** Textos ************/
	
	public String obterTexto(By by) {
		return DriverFactory.getDriver().findElement(by).getText();
	}
	//--------------------------------------------------------------
	
	public String obterTextoPorId(String id) {
		return obterTexto(By.id(id));
	}
	
	public String getTextoPorClassName(String className) {
		return obterTexto(By.className(className));
	}
	
	/* ******** Tabela ************/
	/**
	 * Esse m�todo retorna um string contendo o nome do cliente passado por par�metro;
	 * Ele faz uma busca por cpf na tabela passada por par�metro;
	 * Pega o �ndice da c�lula onde est� o cpf;
	 * Retorna o nome de acordo com o �ndece da c�lula do cpf.
	 * */
	public String getNomePorCpf(String cpfCliente, String xpathTabela){
		WebElement tabela = DriverFactory.getDriver().findElement(By.xpath(xpathTabela));
		List<WebElement> linhasCpf = tabela.findElements(By.xpath(".//tbody/tr/td[2]"));
		int idLinhaCPF = -1;
		for(int i = 0; i < linhasCpf.size(); i++) {
			if(linhasCpf.get(i).getText().equals(cpfCliente)) {
				idLinhaCPF = i+1;
				break;
			}
		}
		WebElement nomeCliente = tabela.findElement(By.xpath(".//tbody/tr["+idLinhaCPF+"]/td[1]"));
		return nomeCliente.getText();
	}			
}
