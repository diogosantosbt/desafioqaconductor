package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import core.BasePage;
import core.DriverFactory;
/**
 * Class responsável ao acesso aos menus e acesso ao base 
 * 
 * */
public class MenuPage extends BasePage{
	
	public void acessarTelaInserirCliente(){
		acessarTela("//span[.='QA']", "//span[.='Clientes']", "//span[.='Incluir']");
	}
	
	public void acessarTelaConsultarCliente(){
		acessarTela("//span[.='QA']", "//span[.='Clientes']", "//a[@title='Clientes']/ancestor::li/ul/li/a/span[.='Listar']");
	}
	
	public void acessarTelaIncluirTransacao() {
		acessarTela("//span[.='QA']", "//span[.='Transações']", "//a[@title='Transações']/ancestor::li/ul/li/a/span[.='Incluir']");
	}
	
	public void acessarTelaListarTransacoes() {
		acessarTela("//span[.='QA']", "//span[.='Transações']", "//a[@title='Transações']/ancestor::li/ul/li/a/span[.='Listar']");
		
	}
	
	public void acessarTela(String xpath1, String xpath2, String xpath3) {
		WebElement dropdown = DriverFactory.getDriver().findElement(By.xpath(xpath1));
		dropdown.click(); 
		Actions action1 = new Actions(DriverFactory.getDriver());
		action1.moveToElement(dropdown).build().perform();
		WebDriverWait wait1 = new WebDriverWait(DriverFactory.getDriver(), 30);
		
		WebElement linkClientes = wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath2)));
		linkClientes.click();
		
		Actions action2 = new Actions(DriverFactory.getDriver());
		action2.moveToElement(linkClientes).build().perform();
		WebDriverWait wait2 = new WebDriverWait(DriverFactory.getDriver(), 30);
		
		WebElement linkIncluir = wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath3)));
		linkIncluir.click();
	}

}
