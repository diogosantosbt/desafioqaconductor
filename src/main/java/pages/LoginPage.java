package pages;

import org.openqa.selenium.By;

import core.BasePage;
import core.DriverFactory;

/**
 * Responsável por setar o login e acesso ao base 
 * */
public class LoginPage extends BasePage {
	
	public void acessarTelaInicial(){
		DriverFactory.getDriver().get("http://provaqa.marketpay.com.br:9084/desafioqa/login");
	}
	
	public void setEmail(String email) {
		escreverPorName("username", email);
	}
	
	public void setSenha(String senha) {
		escreverPorName("password", senha);
	}
	
	public void entrar(){
		clicarBotao(By.xpath("//button[@type='submit']"));
	}
	
	public void logar(String email, String senha) {
		setEmail(email);
		setSenha(senha);
		entrar();
	}

}
