package pages;


import org.openqa.selenium.By;

import core.BasePage;

/**
 * Classe responsável por setar cliente e acesso ao base
 * 
 * */
public class ClientesPage extends BasePage{
	public void setNome(String nome) {
		escreverPorXpath("//input[@id='nome']", nome);
	}
	
	public void setCpf(String cpf) {
		escreverPorXpath("//input[@id='cpf']", cpf);
	}
	
	public void setNome(By by, String texto) {
		escrever(by , texto);
	}
	
	public void setStatus(String valor) {
		selecionarCombo("status", valor);
	}
	
	public void setSaldo(String saldo) {
		escreverID("saldoCliente", saldo);
	}
	
	public void salvar(){
		clicarBotao(By.xpath("//button[@id='botaoSalvar']"));
	}
	
	public String getMsgClienteSalvoSucesso(){
		return obterTexto(By.xpath("//strong[.='Cliente salvo com sucesso']"));
	}
	public String getMsgTransacaoSucess(){
		return obterTexto(By.xpath("//strong[.='Venda realizada com sucesso']"));
	}
	
	public String getMsgBoasVindas() {
		return obterTexto(By.xpath("//h1[@class='page-title txt-color-blueDark']"));
	}

	public void clicarQA() {
		clicar("//span[.='QA']");
	}
	
	public void clicarCliente() {
		clicar("//span[.='Clientes']");
	}
	
	public void clicarIncluir() {
		clicar("//span[.='Incluir']");
	}

	public void setNomeCliente(By id, String nome) {
		escrever(id, nome);
	}
	
	public void setNomeCliente() {
		setNomeCliente(By.id("nome"),"Diogo");
	}
	
	public void setData() {
		setData("18");
	}
	
	public void selecionarCliente(String valor) {
		selecionarCombo("cliente", valor);
	}
	
	public void setValorTransacao(String valor) {
		escreverID("valorTransacao", valor);
	}
	
	public void pesquisar() {
		clicar("//input[@value='Pesquisar']");
	}
	
	public void clicar() {
		clicar("//input[@id='nome']");
	}
	
	public void setNomeConsulta(String string) {
		escreverPorXpath("//input[@class='form-control']", string);
	}

	public String getCpf(String nome) {
		return obterTexto(By.xpath("//tr/td[2][.='000.000.000-00']"));
	}

	public void clicarCalendario() {
		clicar("//input[@id='calendario_input']");
	}

	public void clicarPesquisarClientes() {
		clicar("//input[@value='Pesquisar']");
		
	}

	public String verificarNomeComCpf(String nomeCliente) {
		return getNomePorCpf(nomeCliente, "//table[@class='table table-bordered']");
	}
	
}
