package suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import tests.ClienteTest;
import tests.TransacaoTest;

@RunWith(Suite.class)

@SuiteClasses({
	ClienteTest.class,
	TransacaoTest.class,
})
public class SuiteTestes {

}
