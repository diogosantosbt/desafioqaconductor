package tests;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import core.BaseTest;
import pages.ClientesPage;
import pages.MenuPage;
/**
 * Class de testes em que � testado a venda e consulta de venda;
 * No m�todo testListarTransacao, ao selecionar o cliente, s� � poss�vel visualizar caso
 * selecionar o combobox TODOS. Esse bug foi relatado detalhadamente no relat�rio de bugs.
 * 
 * */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TransacaoTest extends BaseTest{
	MenuPage menuPage = new MenuPage();
	ClientesPage clientePage = new ClientesPage();
	
	@Test
	public void testIncluirTransacao() {
		menuPage.acessarTelaIncluirTransacao();
		clientePage.selecionarCliente("Diogo");
		clientePage.setValorTransacao("10.00");
		clientePage.salvar();
		assertEquals("Venda realizada com sucesso", clientePage.getMsgTransacaoSucess());
	}
	
	@Test
	public void testListarTransacao() {
		menuPage.acessarTelaListarTransacoes();
		clientePage.selecionarCliente("TODOS");
		clientePage.pesquisar();
		assertEquals("Diogo",clientePage.verificarNomeComCpf("000.000.000-00"));
	}
	
}
