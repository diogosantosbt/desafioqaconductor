package tests;


import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import core.DriverFactory;
import core.Propriedades;
import pages.ClientesPage;
import pages.LoginPage;
/**
 * Class de testes onde � testado o login;
 * Aqui foi adicionado o Before e After pois essa class testa unicamente a efetiva��o do login;
 * Nas outras classTests, o Before e After na class BaseTest.
 * 
 * */
public class LoginTest {
	ClientesPage clientePage = new ClientesPage();
	LoginPage loginPage = new LoginPage();
	
	@Test
	public void testLogin() {
		loginPage.acessarTelaInicial();
		loginPage.setEmail("admin");
		loginPage.setSenha("admin");
		loginPage.entrar();
		assertEquals("Bem vindo ao Desafio",clientePage.getMsgBoasVindas());
	}
	
	@Before
	public void inicializa() {
		System.setProperty("webdriver.gecko.driver", "C://geckodriver/geckodriver.exe");
	}
	
	@After
	public void finaliza() throws IOException{
		if(Propriedades.FECHAR_BROWSER) {
			DriverFactory.killDriver();
		}
	}
	
}
