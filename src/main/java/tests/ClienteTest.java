package tests;


import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import core.BaseTest;
import pages.ClientesPage;
import pages.MenuPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClienteTest extends BaseTest {
	
	MenuPage menuPage = new MenuPage();
	ClientesPage clientePage = new ClientesPage();
	
	/**
	 * Class de testes onde testa o cadastro e a consulta de cliente cadastrado
	 */
	
	@Test
	public void testCadastrarCliente() {
		menuPage.acessarTelaInserirCliente();
		clientePage.clicar();
		clientePage.setNome("Diogo");
		clientePage.setCpf("000000000000");
		clientePage.setStatus("Ativo");
		clientePage.setSaldo("10.00");
		clientePage.salvar();
		assertEquals("Cliente salvo com sucesso", clientePage.getMsgClienteSalvoSucesso());
	}
	
	@Test
	public void testConsultarCliente() {
		menuPage.acessarTelaConsultarCliente();
		clientePage.setNomeConsulta("Diogo");
		clientePage.clicarCalendario();
		clientePage.setData();
		clientePage.clicarPesquisarClientes();
		assertEquals("Diogo", clientePage.verificarNomeComCpf("000.000.000-00"));
	}
	
	
}
